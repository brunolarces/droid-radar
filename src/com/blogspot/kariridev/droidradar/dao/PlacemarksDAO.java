package com.blogspot.kariridev.droidradar.dao;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.blogspot.kariridev.droidradar.entity.Placemark;

/**
 * {@link PlacemarksDAO} manipulate the placemark object at database
 * 
 * @author Bruno Lopes Alcantara Batista
 * 
 */
public class PlacemarksDAO extends GenericDAO<Placemark> {

	private static final String TABLE_NAME = "placemarks";

	/**
	 * Default constructor
	 * 
	 * @param context
	 *            The application context
	 */
	public PlacemarksDAO(Context context) {
		super(context);
	}

	@Override
	public long insert(Placemark obj) {
		return db.insert(TABLE_NAME, "description", createContentValues(obj));
	}

	@Override
	public long update(Placemark obj) {
		return db.update(TABLE_NAME, createContentValues(obj), "_id = ?",
				new String[] { obj.getId().toString() });
	}

	@Override
	public long delete(Placemark obj) {
		return db.delete(TABLE_NAME, "_id = ?", new String[] { obj.getId()
				.toString() });
	}

	@Override
	public Placemark load(Integer id) {

		// The placemark variable
		Placemark placemark = null;

		// Execute a database query
		Cursor c = db.query(TABLE_NAME, null, "_id = ?",
				new String[] { id.toString() }, null, null, "_id");

		/*
		 * If there is one register...
		 */
		if (c.getCount() > 0) {

			// Create the placemark object
			placemark = new Placemark();

			// Set the attributes
			placemark.setId(c.getInt(0));
			placemark.setDescription(c.getString(1));
			placemark.setType(c.getString(2));
			placemark.setLongitude(c.getDouble(3));
			placemark.setLatitude(c.getDouble(4));
			placemark.setHeading(c.getInt(5));
			placemark.setRange(c.getInt(6));

		}

		// Return the placemark object
		return placemark;
	}

	@Override
	public List<Placemark> loadAll() {

		// Create the placemark list variable
		List<Placemark> placemarks = null;

		Cursor c = db.query(TABLE_NAME, null, null, null, null, null, "_id");

		if (c.getCount() > 0) {

			placemarks = new ArrayList<Placemark>();

			while (c.moveToNext()) {

				// Create the placemark object
				Placemark placemark = new Placemark();

				// Set the attributes
				placemark.setId(c.getInt(0));
				placemark.setDescription(c.getString(1));
				placemark.setType(c.getString(2));
				placemark.setLongitude(c.getDouble(3));
				placemark.setLatitude(c.getDouble(4));
				placemark.setHeading(c.getInt(5));
				placemark.setRange(c.getInt(6));

				// Add to placemark list
				placemarks.add(placemark);

			}

		}

		// Return the list
		return placemarks;
	}

	public List<Placemark> loadAllByLayer(Integer id) {

		// Create the placemark list variable
		List<Placemark> placemarks = null;

		Cursor c = db.query(TABLE_NAME, null, "layer_id = ?", new String[]{id.toString()}, null, null, "_id");

		if (c.getCount() > 0) {

			placemarks = new ArrayList<Placemark>();

			while (c.moveToNext()) {

				// Create the placemark object
				Placemark placemark = new Placemark();

				// Set the attributes
				placemark.setId(c.getInt(0));
				placemark.setDescription(c.getString(1));
				placemark.setType(c.getString(2));
				placemark.setLongitude(c.getDouble(3));
				placemark.setLatitude(c.getDouble(4));
				placemark.setHeading(c.getInt(5));
				placemark.setRange(c.getInt(6));

				// Add to placemark list
				placemarks.add(placemark);

			}

		}

		// Return the list
		return placemarks;
	}

	@Override
	protected ContentValues createContentValues(Placemark obj) {

		// Create the ContectValues object
		ContentValues values = new ContentValues();

		// Add the attributes
		values.put("_id", obj.getId());
		values.put("description", obj.getDescription());
		values.put("type", obj.getType());
		values.put("longitude", obj.getLongitude());
		values.put("latitude", obj.getLatitude());
		values.put("heading", obj.getHeading());
		values.put("range", obj.getRange());

		// Return it
		return values;
	}

}
