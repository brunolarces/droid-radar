package com.blogspot.kariridev.droidradar.dao;

import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

/**
 * {@link GenericDAO} encapsulates the database basic operations logic for all
 * objects
 * 
 * @author Bruno Lopes Alcantara Batista
 * 
 * @param <T>
 *            The kind of object that will be worked
 */
public abstract class GenericDAO<T> {

	// Contants
	protected static final String DATABASE_NAME = "droidradar";
	protected static final int DATABASE_VERSION = 1;

	// The context apllication
	protected Context context;
	
	// SQLite database Helper
	protected SQLiteHelper dbHelper;

	// SQlite database
	protected SQLiteDatabase db;

	/**
	 * Deafult constructor
	 * 
	 * @param context
	 *            The application context
	 */
	public GenericDAO(Context context) {
		this.context = context;
		dbHelper = new SQLiteHelper(this.context, DATABASE_NAME, DATABASE_VERSION);
		db = dbHelper.getWritableDatabase();
	}

	/**
	 * Insert the object at database
	 * 
	 * @param obj
	 *            The object to store
	 * @return The database record id
	 */
	public abstract long insert(T obj);

	/**
	 * Update the object at database
	 * 
	 * @param obj
	 *            The object to update
	 * @return The database record id
	 */
	public abstract long update(T obj);

	/**
	 * Delete the object at database
	 * 
	 * @param obj
	 *            The object to delete
	 * @return The database record id
	 */
	public abstract long delete(T obj);

	/**
	 * Load a database record
	 * 
	 * @param id
	 *            The database record id
	 * @return The object that represents the record
	 */
	public abstract T load(Integer id);

	/**
	 * Load all records from database
	 * 
	 * @return The {@link List} of objects that represents the database records
	 */
	public abstract List<T> loadAll();
	
	protected abstract ContentValues createContentValues(T obj);

}
