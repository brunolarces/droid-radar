package com.blogspot.kariridev.droidradar.dao;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.blogspot.kariridev.droidradar.entity.Layer;

/**
 * {@link LayerDAO} manipulate the layer object at database
 * 
 * @author Bruno Lopes Alcantara Batista
 * 
 */
public class LayerDAO extends GenericDAO<Layer> {

	private static final String TABLE_NAME = "layers";

	/**
	 * Default constructor
	 * 
	 * @param context
	 *            The application context
	 */
	public LayerDAO(Context context) {
		super(context);
	}

	@Override
	public long insert(Layer obj) {
		return db.insert(TABLE_NAME, "name", createContentValues(obj));
	}

	@Override
	public long update(Layer obj) {
		return db.update(TABLE_NAME, createContentValues(obj), "_id = ?",
				new String[] { obj.getId().toString() });
	}

	@Override
	public long delete(Layer obj) {
		return db.delete(TABLE_NAME, "_id = ?", new String[] { obj.getId()
				.toString() });
	}

	@Override
	public Layer load(Integer id) {

		// Create the layer variable
		Layer layer = null;

		// Execute a database query
		Cursor c = db.query(TABLE_NAME, null, "_id = ?",
				new String[] { id.toString() }, null, null, "_id");

		/*
		 * If there is one register...
		 */
		if (c.getCount() > 0) {

			// Create the layer object
			layer = new Layer();

			// Set the id
			layer.setId(c.getInt(0));

			// Set the name
			layer.setName(c.getString(1));
		}

		// Return the layer object
		return layer;
	}

	public Layer loadWithPlacemakers(Integer id) {

		// Create the layer variable
		Layer layer = null;

		// Execute a database query
		Cursor c = db.query(TABLE_NAME, null, "_id = ?",
				new String[] { id.toString() }, null, null, "_id");

		/*
		 * If there is one register...
		 */
		if (c.getCount() > 0) {

			// Create the layer object
			layer = new Layer();

			// Set the id
			layer.setId(c.getInt(0));

			// Set the name
			layer.setName(c.getString(1));
		}

		// Retrieve the placemarkes of layer
		PlacemarksDAO pdao = new PlacemarksDAO(context);
		layer.setPlacemarks(pdao.loadAllByLayer(layer.getId()));

		// Return the layer object
		return layer;
	}

	@Override
	public List<Layer> loadAll() {

		// Create the layer list variable
		List<Layer> layers = null;

		// Execute a database search
		Cursor c = db.query(TABLE_NAME, null, null, null, null, null, "_id");

		/*
		 * If there is one register...
		 */
		if (c.getColumnCount() > 0) {
			// Create the layer list object
			layers = new ArrayList<Layer>();

			/*
			 * while there is records ro read...
			 */
			while (c.moveToNext()) {

				// Create the layer object
				Layer layer = new Layer();

				// Set the id
				layer.setId(c.getInt(0));

				// Set the name
				layer.setName(c.getString(1));

				// Add to layer list
				layers.add(layer);

			}
		}

		// Return the list
		return layers;
	}

	@Override
	protected ContentValues createContentValues(Layer obj) {

		// Create the ContentValues object
		ContentValues values = new ContentValues();

		// Add the attributes
		values.put("_id", obj.getId());
		values.put("name", obj.getName());

		// Return it
		return values;
	}
}
