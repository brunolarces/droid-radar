package com.blogspot.kariridev.droidradar.dao;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * {@link SQLiteHelper} class encapsulates the database creation logic
 * 
 * @author Bruno Lopes Alcantara Batista
 */
public class SQLiteHelper extends SQLiteOpenHelper {

	private static final String LOG_TAG = SQLiteHelper.class.getSimpleName();
	private Context context;
	private String name;
	private int version;

	/**
	 * Default constructor
	 * 
	 * @param context
	 *            The application {@link Context}
	 * @param name
	 *            The database name
	 * @param version
	 *            The database version
	 */
	public SQLiteHelper(Context context, String name, int version) {
		super(context, name, null, version);

		this.context = context;
		this.name = name;
		this.version = version;

	}

	@Override
	public void onCreate(SQLiteDatabase db) {

		try {
			
			// Log the operation
			Log.i(LOG_TAG, "Initialize the database creation proccess");
			Log.i(LOG_TAG, "Database name: " + name);
			Log.i(LOG_TAG, "Database version: " + version);

			// Get the SQL File InputStream
			InputStream inputStream = context.getAssets()
					.open("droidradar.sql");

			// Create the BufferedReader of File
			BufferedReader bufferedReader = new BufferedReader(
					new InputStreamReader(inputStream));

			// Auxiliary variable
			String line = "";

			while ((line = bufferedReader.readLine()) != null) {

				// Execute the SQL
				db.execSQL(line);
				Log.i(LOG_TAG, "Executed SQL: " + line);

			}
			
			// Log the operation
			Log.i(LOG_TAG, "Finished the database creation proccess");

		} catch (IOException e) {
			Log.e(LOG_TAG, "Error to open the file: " + e.getMessage());
			e.printStackTrace();
		}

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

		if (newVersion > oldVersion) {

			// Drop the database
			Log.i(LOG_TAG, "Drop database version");
			db.execSQL("DROP TABLE IF EXISTS placemarks");
			db.execSQL("DROP TABLE IF EXISTS layers");

			// Create it again
			onCreate(db);

		} else {
			// Log the operation
			Log.i(LOG_TAG, "Nothing to do!");
		}

	}

}
