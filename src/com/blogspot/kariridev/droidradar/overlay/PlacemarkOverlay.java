package com.blogspot.kariridev.droidradar.overlay;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.RectF;
import android.view.MotionEvent;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;

/******************************************************************************
 * {@link PlacemarkOverlayItem} class represent a custom overlay for map view
 * component
 * 
 * @author Bruno Lopes Alcantara Batista
 * 
 ******************************************************************************/
public class PlacemarkOverlay extends Overlay {

	// The paint object
	private Paint paint;

	// The resourc id
	private final int resId;

	// The geopoint object
	private final GeoPoint geoPoint;

	// Boolean value
	private boolean isDraw;

	/**
	 * The {@link PlacemarkOverlayItem} contructor
	 * 
	 * @param geoPoint
	 *            The geopoint to put the overlay
	 * @param resId
	 *            The resource id of overlay image
	 */
	public PlacemarkOverlay(GeoPoint geoPoint, int resId) {
		this.geoPoint = geoPoint;
		this.resId = resId;
		this.isDraw = true;
	}

	@Override
	public void draw(Canvas canvas, MapView mapView, boolean shadow) {

		super.draw(canvas, mapView, shadow);

		/*
		 * If isDraw is true, draw the overlay on map
		 */
		if (isDraw) {

			// Get the point of overlay
			Point p = mapView.getProjection().toPixels(geoPoint, null);

			// Get the image bitmap
			Bitmap bitmap = BitmapFactory.decodeResource(
					mapView.getResources(), this.resId);

			// Create the image rectangle
			RectF r = new RectF(p.x, p.y, p.x + bitmap.getWidth(), p.y
					+ bitmap.getHeight());

			// Draw the rectangle at screen
			canvas.drawBitmap(bitmap, null, r, paint);

		}

	}

	@Override
	public boolean onTouchEvent(MotionEvent e, MapView mapView) {

		/*
		 * If the user finger is touch the screen..
		 */
		if (e.getAction() == MotionEvent.ACTION_DOWN) {

			// Change the boolean value to false
			isDraw = false;

		}
		/*
		 * If the user finger is not touch the screen..
		 */
		else if (e.getAction() == MotionEvent.ACTION_UP) {

			// Change the boolean value to false
			isDraw = true;
		}

		return super.onTouchEvent(e, mapView);
	}

	@Override
	public boolean onTap(GeoPoint p, MapView mapView) {
		return super.onTap(p, mapView);
	}

}
