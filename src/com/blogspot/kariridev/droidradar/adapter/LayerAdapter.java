package com.blogspot.kariridev.droidradar.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.blogspot.kariridev.droidradar.R;
import com.blogspot.kariridev.droidradar.entity.Layer;
import com.blogspot.kariridev.droidradar.interfaces.OnLayerSelected;
import com.blogspot.kariridev.droidradar.interfaces.SubjectOnLayerSelected;

/**
 * {@link LayerAdapter} class is a custom adapter layer object for list view
 * component
 * 
 * @author Bruno Lopes Alcantara Batista
 * 
 */
public class LayerAdapter extends BaseAdapter implements SubjectOnLayerSelected {

	// The application context
	private Context context;

	// The layers list
	private List<Layer> layers;

	// The observer reference
	private OnLayerSelected obj;

	/**
	 * Default LayerAdapter contructor
	 * 
	 * @param obj
	 *            The reference of class
	 * @param context
	 *            The application context
	 * @param layers
	 *            The layers list
	 */
	public LayerAdapter(OnLayerSelected obj, Context context, List<Layer> layers) {
		this.obj = obj;
		this.context = context;
		this.layers = layers;
	}

	/**
	 * Get layer list count size
	 */
	public int getCount() {
		return layers.size();
	}

	/**
	 * Get the layer list item
	 */
	public Object getItem(int position) {
		return layers.get(position);
	}

	/**
	 * Get the layer list item id
	 */
	public long getItemId(int position) {
		return layers.indexOf(layers.get(position));
	}

	/**
	 * Create the view object to populate the listview
	 */
	public View getView(int position, View convertView, ViewGroup parent) {

		// Static class to store the line item
		ViewHolder holder;

		// The layer object
		final Layer l = layers.get(position);

		/*
		 * If convertView is null...
		 */
		if (convertView == null) {

			// Inflate the view and set on cenvert view
			convertView = LayoutInflater.from(context).inflate(
					R.layout.item_layer_list, null);

			// Create the holder object
			holder = new ViewHolder();

			// Set the attributes
			holder.image = (ImageView) convertView.findViewById(R.item.image);
			holder.label = (TextView) convertView.findViewById(R.item.label);
			holder.checkbox = (CheckBox) convertView
					.findViewById(R.item.checkbox);

			// Set tag
			convertView.setTag(holder);

		} else {

			// Get tag
			holder = (ViewHolder) convertView.getTag();
		}

		// Set the image value
		holder.image.setImageResource(getImage(l.getName()));

		// Set the text values
		holder.label.setText(l.getName());

		// Set the checkbox and it listener
		holder.checkbox.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {

				// Get the checkbox reference
				CheckBox chk = (CheckBox) v;

				/*
				 * If the checkbox is checked...
				 */
				if (chk.isChecked()) {

					// Notify the observer
					notifySelectedLayer(l);

					// Force the checkbox check
					chk.setChecked(true);

				}
				/*
				 * If the checkbox is unchecked
				 */
				else {

					// Notify the observer
					notifyUnselectedLayer(l);

					// Force the checkbox uncheck
					chk.setChecked(false);
				}

			}
		});

		// Return the convert view object
		return convertView;
	}

	/**
	 * Static class to store the listview lines
	 * 
	 * @author Bruno Lopes
	 * 
	 */
	static class ViewHolder {
		ImageView image;
		TextView label;
		CheckBox checkbox;
	}

	public void notifySelectedLayer(Layer l) {
		if (l != null) {
			obj.onLayerSelected(l);
		}
	}

	public void notifyUnselectedLayer(Layer l) {
		if (l != null) {
			obj.onLayerUnselected(l);
		}
	}

	/**
	 * Get the image resource id
	 * 
	 * @param type
	 *            The type of layer image
	 * @return The image resource id
	 */
	private int getImage(String type) {

		// Set the standard image
		int result = R.drawable.ic_launcher;

		/*
		 * If the type is not null and is not empty...
		 */
		if (type != null && !type.trim().equals("")) {

			/*
			 * Choose the image id
			 */
			if (type.equals("Radar Fixo")) {
				result = R.drawable.fixo60;
			} else if (type.equals("Radar Movel")) {
				result = R.drawable.movel60;
			} else if (type.equals("Semaforo com Camera")) {
				result = R.drawable.semaforo;
			} else if (type.equals("Semaforo com Radar")) {
				result = R.drawable.semaforo60;
			} else if (type.equals("Policia Rodoviaria")) {
				result = R.drawable.prf;
			} else if (type.equals("Pedagio")) {
				result = R.drawable.pedagio;
			} else if (type.equals("Lombada")) {
				result = R.drawable.lombada;
			}

		}

		// Return the result
		return result;

	}

}
