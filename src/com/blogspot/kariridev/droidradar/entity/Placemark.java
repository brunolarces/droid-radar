package com.blogspot.kariridev.droidradar.entity;

import android.graphics.Point;

/**
 * {@link Placemark} class represents a Placemark
 * 
 * @author Bruno Lopes Alcantara Batista
 * @see {@link LookAt}
 * @see {@link Point}
 */
public class Placemark {

	// The placemark id
	private Integer id;

	// The placemark description
	private String description;

	// The placemark type
	private String type;

	// The longitude value
	private double longitude;

	// The Latitude value
	private double latitude;

	// The compass heading value
	private int heading;

	// The range value
	private int range;

	/**
	 * Default Constructor
	 */
	public Placemark() {
		// TODO Auto-generated constructor stub
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public int getHeading() {
		return heading;
	}

	public void setHeading(int heading) {
		this.heading = heading;
	}

	public int getRange() {
		return range;
	}

	public void setRange(int range) {
		this.range = range;
	}

}
