package com.blogspot.kariridev.droidradar.entity;

/**
 * Enum with the drawable images names
 * 
 * @author Bruno Lopes Alcantara Batista
 * 
 */
public enum DrawableEnum {
	fixo30, fixo40, fixo50, fixo60, fixo70, fixo80, fixo90, fixo100, fixo110, fixo120, lombada, movel30, movel40, movel50, movel60, movel70, movel80, movel90, movel100, movel110, movel120, pedagio, perigo, prf, semaforo, semaforo30, semaforo40, semaforo50, semaforo60, semaforo70, semaforo80
}
