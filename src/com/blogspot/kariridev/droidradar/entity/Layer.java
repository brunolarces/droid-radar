package com.blogspot.kariridev.droidradar.entity;

import java.util.List;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * 
 * {@link Layer} class represent a layer
 * 
 * @author Bruno Lopes Alcantara Batista
 * 
 */
public class Layer implements Parcelable{

	// The layer id
	private Integer id;

	// The name of the Folder
	private String name;

	// The placemarks of folder
	private List<Placemark> placemarks;

	/**
	 * Default Constructor
	 */
	public Layer() {

	}
	
	private Layer(Parcel source){
		this.id = source.readInt();
		this.name = source.readString();
		this.placemarks = null;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Placemark> getPlacemarks() {
		return placemarks;
	}

	public void setPlacemarks(List<Placemark> placemarks) {
		this.placemarks = placemarks;
	}

	public int describeContents() {
		return 0;
	}

	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(id);
		dest.writeString(name);
	}
	
	public static final Parcelable.Creator<Layer> CREATOR = new Creator<Layer>() {
		
		public Layer[] newArray(int size) {
			return new Layer[size];
		}
		
		public Layer createFromParcel(Parcel source) {
			return new Layer(source);
		}
	};

}
