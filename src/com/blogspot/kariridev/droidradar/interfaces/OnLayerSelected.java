package com.blogspot.kariridev.droidradar.interfaces;

import com.blogspot.kariridev.droidradar.entity.Layer;

/******************************************************************************
 * Interface that implements the Observer Patterns
 * 
 * @author Bruno Lopes Alcantara Batista
 * 
 ******************************************************************************/
public interface OnLayerSelected {

	/**
	 * Call when user select a layer
	 * @param l The layer selected
	 */
	public void onLayerSelected(Layer l);

	/**
	 * Call when user unselect a layer
	 * @param l The layer unselected
	 */
	public void onLayerUnselected(Layer l);

}
