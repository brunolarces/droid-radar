package com.blogspot.kariridev.droidradar.interfaces;

import com.blogspot.kariridev.droidradar.entity.Layer;

/******************************************************************************
 * Interface that implements the Observer Patterns
 * 
 * @author Bruno Lopes Alcantara Batista
 * 
 ******************************************************************************/
public interface SubjectOnLayerSelected {

	public void notifySelectedLayer(Layer l);

	public void notifyUnselectedLayer(Layer l);

}
