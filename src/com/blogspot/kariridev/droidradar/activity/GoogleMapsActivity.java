package com.blogspot.kariridev.droidradar.activity;

import java.util.List;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

import com.blogspot.kariridev.droidradar.R;
import com.blogspot.kariridev.droidradar.dao.PlacemarksDAO;
import com.blogspot.kariridev.droidradar.entity.DrawableEnum;
import com.blogspot.kariridev.droidradar.entity.Layer;
import com.blogspot.kariridev.droidradar.entity.Placemark;
import com.blogspot.kariridev.droidradar.overlay.PlacemarkOverlay;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.MyLocationOverlay;

/******************************************************************************
 * {@link GoogleMapsActivity} is the activity who user use to view the points of
 * selected layers
 * 
 * @author Bruno Lopes Alcantara Batista
 * 
 ******************************************************************************/
public class GoogleMapsActivity extends MapActivity implements LocationListener {

	// Layers list
	private List<Layer> layers;

	// Mapview component
	private MapView mapView;

	// MapController object
	private MapController controller;

	// LocationManager object
	private LocationManager locationManager;

	// MyLocationOverlay object
	private MyLocationOverlay myLocation;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_google_map);

		// Get the location manager reference
		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

		// Get the MapView component reference
		mapView = (MapView) findViewById(R.google_map.map_view);

		// Enable the built zoom control
		mapView.setBuiltInZoomControls(true);

		// Get the MapController reference
		controller = mapView.getController();

		// Set the default zoom
		controller.setZoom(15);

		// Create the Mylocation object. This object will show the user location
		// on map
		myLocation = new MyLocationOverlay(this, mapView);

		// Get the selected layers at previous activity
		layers = getIntent().getParcelableArrayListExtra("layers");

		// The placemark dao
		PlacemarksDAO pdao = new PlacemarksDAO(this);

		// Interate over layers list
		for (int i = 0; i < layers.size(); i++) {

			// Get the layer id
			int id = layers.get(i).getId();

			// Get the placemarker of the layer
			List<Placemark> placemarks = pdao.loadAllByLayer(id);

			// Set the placemarkers list on the layer
			layers.get(i).setPlacemarks(placemarks);
		}

	}

	@Override
	protected void onStart() {
		super.onStart();

		// Request the GPS location updates
		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
				2000, 25, this);

		// Set the mylocation overlay at map
		mapView.getOverlays().add(myLocation);

	}

	@Override
	protected void onResume() {
		super.onResume();

		// Enable my location
		myLocation.enableMyLocation();

		// Enable compass
		myLocation.enableCompass();
	}

	@Override
	protected void onPause() {
		super.onPause();

		// Disable my location
		myLocation.disableMyLocation();

		// Disable compass
		myLocation.disableCompass();
	}

	@Override
	protected void onStop() {
		super.onStop();

		// Remove location updates
		locationManager.removeUpdates(this);
	}

	@Override
	protected boolean isRouteDisplayed() {
		return false;
	}

	/**
	 * Get the correct image from the placemark type
	 * 
	 * @param type
	 *            The placemark type
	 * @return The resource id of image
	 */
	private int getDrawableImage(String type) {
		int result = R.drawable.ic_launcher;

		if (DrawableEnum.fixo30.name().equals(type)) {
			result = R.drawable.fixo30;

		} else if (DrawableEnum.fixo40.name().equals(type)) {
			result = R.drawable.fixo40;

		} else if (DrawableEnum.fixo50.name().equals(type)) {
			result = R.drawable.fixo50;

		} else if (DrawableEnum.fixo60.name().equals(type)) {
			result = R.drawable.fixo60;

		} else if (DrawableEnum.fixo70.name().equals(type)) {
			result = R.drawable.fixo70;

		} else if (DrawableEnum.fixo80.name().equals(type)) {
			result = R.drawable.fixo80;

		} else if (DrawableEnum.fixo90.name().equals(type)) {
			result = R.drawable.fixo90;

		} else if (DrawableEnum.fixo100.name().equals(type)) {
			result = R.drawable.fixo100;

		} else if (DrawableEnum.fixo110.name().equals(type)) {
			result = R.drawable.fixo110;

		} else if (DrawableEnum.fixo120.name().equals(type)) {
			result = R.drawable.fixo120;

		} else if (DrawableEnum.movel30.name().equals(type)) {
			result = R.drawable.movel30;

		} else if (DrawableEnum.movel40.name().equals(type)) {
			result = R.drawable.movel40;

		} else if (DrawableEnum.movel50.name().equals(type)) {
			result = R.drawable.movel50;

		} else if (DrawableEnum.movel60.name().equals(type)) {
			result = R.drawable.movel60;

		} else if (DrawableEnum.movel70.name().equals(type)) {
			result = R.drawable.movel70;

		} else if (DrawableEnum.movel80.name().equals(type)) {
			result = R.drawable.movel80;

		} else if (DrawableEnum.movel90.name().equals(type)) {
			result = R.drawable.movel90;

		} else if (DrawableEnum.movel100.name().equals(type)) {
			result = R.drawable.movel100;

		} else if (DrawableEnum.movel110.name().equals(type)) {
			result = R.drawable.movel110;

		} else if (DrawableEnum.movel120.name().equals(type)) {
			result = R.drawable.movel120;

		} else if (DrawableEnum.lombada.name().equals(type)) {
			result = R.drawable.lombada;

		} else if (DrawableEnum.pedagio.name().equals(type)) {
			result = R.drawable.pedagio;

		} else if (DrawableEnum.perigo.name().equals(type)) {
			result = R.drawable.perigo;

		} else if (DrawableEnum.prf.name().equals(type)) {
			result = R.drawable.prf;

		} else if (DrawableEnum.semaforo.name().equals(type)) {
			result = R.drawable.semaforo;

		} else if (DrawableEnum.semaforo30.name().equals(type)) {
			result = R.drawable.semaforo30;

		} else if (DrawableEnum.semaforo40.name().equals(type)) {
			result = R.drawable.semaforo40;

		} else if (DrawableEnum.semaforo50.name().equals(type)) {
			result = R.drawable.semaforo50;

		} else if (DrawableEnum.semaforo60.name().equals(type)) {
			result = R.drawable.semaforo60;

		} else if (DrawableEnum.semaforo70.name().equals(type)) {
			result = R.drawable.semaforo70;

		} else if (DrawableEnum.semaforo80.name().equals(type)) {
			result = R.drawable.semaforo80;
		}

		return result;
	}

	/**
	 * The listener of location changed
	 */
	public void onLocationChanged(Location location) {

		// Calculate the decimal latitude of user location
		int latE6 = (int) (location.getLatitude() * 1E6);

		// Calculate the decimal longitude of user location
		int lonE6 = (int) (location.getLongitude() * 1E6);

		// Get the geopoint object
		GeoPoint g = new GeoPoint(latE6, lonE6);

		// Iterate over layers list
		for (Layer l : layers) {

			// Iterate over placemarks list
			for (Placemark p : l.getPlacemarks()) {

				// Calculate the distance between user location and placemark
				double distance = getDistanceBetween(location.getLatitude(),
						location.getLongitude(), p.getLatitude(),
						p.getLongitude());

				/*
				 * If the distance of user location and placemark is less than
				 * 2000 meter...
				 */
				if (distance < 2000) {

					// Calculate the decimal latitude of placemark
					latE6 = (int) (p.getLatitude() * 1E6);

					// Calculate the decimal longitude of placemark
					lonE6 = (int) (p.getLongitude() * 1E6);

					// Create the geopoint of placemark
					GeoPoint geoPoint = new GeoPoint(latE6, lonE6);

					// Create the placemark overlay
					PlacemarkOverlay pol = new PlacemarkOverlay(geoPoint,
							getDrawableImage(p.getType()));

					// Add the placemark overlay at map
					mapView.getOverlays().add(pol);

				}

			}
		}

		// Animate the map to user location
		controller.animateTo(g);

		// Redraw the map
		mapView.invalidate();

	}

	/**
	 * Calculate the distance between two points at earth using the latitude and
	 * longitude coordinates
	 * 
	 * @param lat1
	 *            Latitude of point 1
	 * @param lon1
	 *            Longitude of pont 1
	 * @param lat2
	 *            Latitude of pont 2
	 * @param lon2
	 *            Logitude of point 2
	 * @return The distance in meters
	 */
	private double getDistanceBetween(double lat1, double lon1, double lat2,
			double lon2) {

		double radLat1 = lat1 * Math.PI / 180;
		double radLon1 = lon1 * Math.PI / 180;
		double radLat2 = lat2 * Math.PI / 180;
		double radLon2 = lon2 * Math.PI / 180;

		double deltaLat = radLat2 - radLat1;
		double deltaLon = radLon2 - radLon1;

		double a = Math.sin(deltaLat / 2) * Math.sin(deltaLat / 2)
				+ Math.cos(radLat1) * Math.cos(radLat2)
				* Math.sin(deltaLon / 2) * Math.sin(deltaLon / 2);

		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

		return Math.round(6371.0 * c * 1000.0);

	}

	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub

	}

	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub

	}

	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub

	}

}
