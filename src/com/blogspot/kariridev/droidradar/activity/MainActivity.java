package com.blogspot.kariridev.droidradar.activity;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;

import com.blogspot.kariridev.droidradar.R;
import com.blogspot.kariridev.droidradar.adapter.LayerAdapter;
import com.blogspot.kariridev.droidradar.dao.LayerDAO;
import com.blogspot.kariridev.droidradar.entity.Layer;
import com.blogspot.kariridev.droidradar.interfaces.OnLayerSelected;

/******************************************************************************
 * 
 * {@link MainActivity} is the first activity to be displayed to user
 * 
 * @author Bruno Lopes Alcantara Batista
 * 
 ******************************************************************************/
public class MainActivity extends FragmentActivity implements OnLayerSelected,
		OnClickListener {

	// Store the selected layers
	private List<Layer> selectedLayers;
	
	// The list view component
	private ListView listView;
	
	// the button component
	private Button btViewMap;

	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// Get the listView reference
		listView = (ListView) findViewById(R.layer_list.list_view);

		//Get the button reference and set the click listener
		btViewMap = (Button) findViewById(R.main.bt_view_map);
		btViewMap.setOnClickListener(this);

	}

	@Override
	protected void onStart() {
		super.onStart();

		// Initiate the selected layers list
		selectedLayers = new ArrayList<Layer>();

		// Get all layers on database
		LayerDAO ldao = new LayerDAO(this.getBaseContext());
		List<Layer> layers = ldao.loadAll();

		// Set the layer adapter at list view
		listView.setAdapter(new LayerAdapter(this, this.getBaseContext(),
				layers));

	}

	public void onLayerSelected(Layer l) {
		if (l != null) {
			selectedLayers.add(l);
		}
	}

	public void onLayerUnselected(Layer l) {
		if (l != null && selectedLayers.contains(l)) {
			selectedLayers.remove(l);
		}
	}

	public void onClick(View v) {

		// Get the component ID
		switch (v.getId()) {
		
		// If the component is button with id bt_view_map
		case R.main.bt_view_map:
			
			// Create the new intent
			Intent it = new Intent(this, GoogleMapsActivity.class);
			
			// Attach the selected layers list on intent
			it.putParcelableArrayListExtra("layers",
					(ArrayList<? extends Parcelable>) selectedLayers);
			
			// Start the map activity
			startActivity(it);
			break;
		}

	}

}
