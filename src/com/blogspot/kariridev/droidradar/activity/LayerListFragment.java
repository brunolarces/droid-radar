package com.blogspot.kariridev.droidradar.activity;

import com.blogspot.kariridev.droidradar.R;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/******************************************************************************
 * {@link LayerListFragment} is a fragment that represents a ListView
 * @author Bruno Lopes Alcantara Batista
 *
 ******************************************************************************/
public class LayerListFragment extends Fragment{

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		return inflater.inflate(R.layout.fragment_layer_list, container, false);
	}
	
}
